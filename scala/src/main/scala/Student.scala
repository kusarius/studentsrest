case class Student(id: Int,
                   name: String,
                   age: Int,
                   averageMark: Float)
