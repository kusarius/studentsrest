package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

var (
	students = make([]Student, 0)
)

const (
	bindAddress = "localhost:8080"
)

func indexWhere(s []Student, f func(Student) bool) int {
	for i, el := range s {
		if f(el) {
			return i
		}
	}
	return -1
}

func respondJSON(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
	w.WriteHeader(http.StatusOK)
}

func getAllStudents(w http.ResponseWriter, r *http.Request) {
	respondJSON(w, students)
}

func getStudentById(w http.ResponseWriter, r *http.Request) {
	id, _ := strconv.Atoi(mux.Vars(r)["id"])
	index := indexWhere(students, func(s Student) bool { return s.ID == id })

	if index == -1 {
		w.WriteHeader(http.StatusNotFound)
	} else {
		respondJSON(w, students[index])
	}
}

func createStudent(w http.ResponseWriter, r *http.Request) {
	var student Student
	if json.NewDecoder(r.Body).Decode(&student) != nil {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		students = append(students, student)
		w.WriteHeader(http.StatusOK)
	}
}

func editStudent(w http.ResponseWriter, r *http.Request) {
	id, _ := strconv.Atoi(mux.Vars(r)["id"])
	index := indexWhere(students, func(s Student) bool { return s.ID == id })

	if index == -1 {
		w.WriteHeader(http.StatusNotFound)
	} else {
		var student Student
		if json.NewDecoder(r.Body).Decode(&student) != nil {
			w.WriteHeader(http.StatusInternalServerError)
		} else {
			students[index] = student
			w.WriteHeader(http.StatusOK)
		}
	}
}

func deleteStudent(w http.ResponseWriter, r *http.Request) {
	id, _ := strconv.Atoi(mux.Vars(r)["id"])
	index := indexWhere(students, func(s Student) bool { return s.ID == id })

	if index == -1 {
		w.WriteHeader(http.StatusNotFound)
	} else {
		students = append(students[:index], students[index+1:]...)
		w.WriteHeader(http.StatusOK)
	}
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/students", getAllStudents).Methods("GET")
	router.HandleFunc("/students/{id:[0-9]+}", getStudentById).Methods("GET")
	router.HandleFunc("/students", createStudent).Methods("POST")
	router.HandleFunc("/students/{id:[0-9]+}", editStudent).Methods("PUT")
	router.HandleFunc("/students/{id:[0-9]+}", deleteStudent).Methods("DELETE")

	fmt.Printf("Listening on %s\n", bindAddress)
	fmt.Println("Press Ctrl-C to stop")

	http.ListenAndServe(bindAddress, router)
}
