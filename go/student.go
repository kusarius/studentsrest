package main

type Student struct {
	ID          int     `json:"id"`
	Name        string  `json:"name"`
	Age         int     `json:"age"`
	AverageMark float32 `json:"averageMark"`
}
